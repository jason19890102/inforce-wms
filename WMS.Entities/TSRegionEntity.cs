using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_region")]
    public class TSRegionEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///城市名
        ///</summary>
        public string NAME { get; set; }

        ///<summary>
        ///父ID
        ///</summary>
        public string PID { get; set; }

        ///<summary>
        ///英文名
        ///</summary>
        public string NAME_EN { get; set; }


    }


}
