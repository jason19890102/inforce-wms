using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_log")]
    public class TSLogEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///浏览器
        ///</summary>
        public string broswer { get; set; }

        ///<summary>
        ///日志内容
        ///</summary>
        public string logcontent { get; set; }

        ///<summary>
        ///日志级别
        ///</summary>
        public short? loglevel { get; set; }

        ///<summary>
        ///IP
        ///</summary>
        public string note { get; set; }

        ///<summary>
        ///操作时间
        ///</summary>
        public DateTime operatetime { get; set; }

        ///<summary>
        ///操作类型
        ///</summary>
        public short? operatetype { get; set; }

        ///<summary>
        ///用户ID
        ///</summary>
        public string userid { get; set; }


    }


}
