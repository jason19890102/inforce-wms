using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wms_app_function")]
    public class WmsAppFunctionEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///app模块编号
        ///</summary>
        public string appmodel_code { get; set; }

        ///<summary>
        ///app模块名称
        ///</summary>
        public string appmodel_name { get; set; }

        ///<summary>
        ///app模块排序
        ///</summary>
        public string appmodel_sort { get; set; }

        ///<summary>
        ///类型
        ///</summary>
        public string type { get; set; }

        ///<summary>
        ///路径
        ///</summary>
        public string route { get; set; }

        ///<summary>
        ///图片
        ///</summary>
        public string picture { get; set; }

        ///<summary>
        ///是否禁用
        ///</summary>
        public string if_bind { get; set; }

        ///<summary>
        ///备用1
        ///</summary>
        public string query1 { get; set; }

        ///<summary>
        ///备用2
        ///</summary>
        public string query2 { get; set; }

        ///<summary>
        ///备用3
        ///</summary>
        public string query3 { get; set; }

        ///<summary>
        ///备用4
        ///</summary>
        public string query4 { get; set; }

        ///<summary>
        ///备用5
        ///</summary>
        public string query5 { get; set; }

        ///<summary>
        ///备用6
        ///</summary>
        public string query6 { get; set; }


    }


}
