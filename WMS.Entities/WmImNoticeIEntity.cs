using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_im_notice_i")]
    public class WmImNoticeIEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///到货通知单号
        ///</summary>
        public string im_notice_id { get; set; }

        ///<summary>
        ///到货通知项目
        ///</summary>
        public string im_notice_item { get; set; }

        ///<summary>
        ///物料编码
        ///</summary>
        public string goods_code { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string goods_count { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public DateTime? goods_prd_data { get; set; }

        ///<summary>
        ///批次
        ///</summary>
        public string goods_batch { get; set; }

        ///<summary>
        ///库位整理
        ///</summary>
        public string bin_pre { get; set; }

        ///<summary>
        ///体积
        ///</summary>
        public string goods_fvol { get; set; }

        ///<summary>
        ///重量
        ///</summary>
        public string goods_weight { get; set; }

        ///<summary>
        ///计划库位
        ///</summary>
        public string bin_plan { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///未清数量
        ///</summary>
        public string goods_wqm_count { get; set; }

        ///<summary>
        ///收货登记数量
        ///</summary>
        public string goods_qm_count { get; set; }

        ///<summary>
        ///行项目状态
        ///</summary>
        public string noticei_sta { get; set; }

        ///<summary>
        ///基本单位
        ///</summary>
        public string base_unit { get; set; }

        ///<summary>
        ///基本单位数量
        ///</summary>
        public string base_goodscount { get; set; }

        ///<summary>
        ///基本单位收货数量
        ///</summary>
        public string base_qmcount { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string goods_name { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string other_id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string im_cus_code { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string im_beizhu { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string barcode { get; set; }

        ///<summary>
        ///规格
        ///</summary>
        public string shp_gui_ge { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string BZHI_QI { get; set; }

        ///<summary>
        ///产品属性
        ///</summary>
        public string chp_shu_xing { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string tin_id { get; set; }


    }


}
