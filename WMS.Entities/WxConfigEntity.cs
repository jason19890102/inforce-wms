using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wx_config")]
    public class WxConfigEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///流程状态
        ///</summary>
        public string bpm_status { get; set; }

        ///<summary>
        ///前端编码
        ///</summary>
        public string app_code { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string app_remark { get; set; }

        ///<summary>
        ///appID
        ///</summary>
        public string app_id { get; set; }

        ///<summary>
        ///appsecret
        ///</summary>
        public string app_secret { get; set; }

        ///<summary>
        ///appkey
        ///</summary>
        public string app_key { get; set; }

        ///<summary>
        ///商户号
        ///</summary>
        public string mch_id { get; set; }

        ///<summary>
        ///通知地址
        ///</summary>
        public string notify_url { get; set; }

        ///<summary>
        ///GRANT_TYPE
        ///</summary>
        public string grant_type { get; set; }

        ///<summary>
        ///备用1
        ///</summary>
        public string wx_by1 { get; set; }

        ///<summary>
        ///备用2
        ///</summary>
        public string wx_by2 { get; set; }

        ///<summary>
        ///备用3
        ///</summary>
        public string wx_by3 { get; set; }

        ///<summary>
        ///备用4
        ///</summary>
        public string wx_by4 { get; set; }

        ///<summary>
        ///备用5
        ///</summary>
        public string wx_by5 { get; set; }


    }


}
