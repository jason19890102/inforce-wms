﻿using System.Collections.Generic;

namespace WMS.Configuration
{
    public class ConfigInfoEntity
    {
        public string WMSDB { get; set; }
        
        public List<ConfigDictItem> dictCompTypes { get; set; }
    }
}