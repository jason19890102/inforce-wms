using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 600064 kB; (`parentfunctionid`) REFER `jeecg/t_
    ///</summary>
    [Table("t_s_function")]
    public class TSFunctionEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///菜单地址打开方式
        ///</summary>
        public short? functioniframe { get; set; }

        ///<summary>
        ///菜单等级
        ///</summary>
        public short? functionlevel { get; set; }

        ///<summary>
        ///菜单名字
        ///</summary>
        public string functionname { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public string functionorder { get; set; }

        ///<summary>
        ///URL
        ///</summary>
        public string functionurl { get; set; }

        ///<summary>
        ///父菜单ID
        ///</summary>
        public string parentfunctionid { get; set; }

        ///<summary>
        ///图标ID
        ///</summary>
        public string iconid { get; set; }

        ///<summary>
        ///桌面图标ID
        ///</summary>
        public string desk_iconid { get; set; }

        ///<summary>
        ///菜单类型
        ///</summary>
        public short? functiontype { get; set; }

        ///<summary>
        ///ace图标样式
        ///</summary>
        public string function_icon_style { get; set; }

        ///<summary>
        ///创建人id
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///修改人id
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///修改时间
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///修改人
        ///</summary>
        public string update_name { get; set; }


    }


}
