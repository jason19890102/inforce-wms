using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_om_qm_i")]
    public class WmOmQmIEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///到货通知单
        ///</summary>
        public string om_notice_id { get; set; }

        ///<summary>
        ///到货通知行项目
        ///</summary>
        public string iom_notice_item { get; set; }

        ///<summary>
        ///物料编码
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///出货数量
        ///</summary>
        public string om_quat { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string qm_ok_quat { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string item_text { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public string pro_data { get; set; }

        ///<summary>
        ///托盘
        ///</summary>
        public string tin_id { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///批次
        ///</summary>
        public string goods_batch { get; set; }

        ///<summary>
        ///仓位
        ///</summary>
        public string bin_id { get; set; }

        ///<summary>
        ///体积
        ///</summary>
        public string tin_tj { get; set; }

        ///<summary>
        ///重量
        ///</summary>
        public string tin_zhl { get; set; }

        ///<summary>
        ///是否已下架
        ///</summary>
        public string bin_sta { get; set; }

        ///<summary>
        ///货主
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///温度
        ///</summary>
        public string rec_deg { get; set; }

        ///<summary>
        ///任务接收人
        ///</summary>
        public string assign_to { get; set; }

        ///<summary>
        ///基本单位
        ///</summary>
        public string base_unit { get; set; }

        ///<summary>
        ///基本单位数量
        ///</summary>
        public string base_goodscount { get; set; }

        ///<summary>
        ///客户名称
        ///</summary>
        public string cus_name { get; set; }

        ///<summary>
        ///物料名称
        ///</summary>
        public string goods_name { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string wave_id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string im_cus_code { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string om_bei_zhu { get; set; }

        ///<summary>
        ///条码
        ///</summary>
        public string BARCODE { get; set; }

        ///<summary>
        ///保质期
        ///</summary>
        public string baozhiqi { get; set; }

        ///<summary>
        ///规格
        ///</summary>
        public string shp_gui_ge { get; set; }

        ///<summary>
        ///拣货换算
        ///</summary>
        public string pick_notice { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string first_rq { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string second_rq { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string sku { get; set; }


    }


}
