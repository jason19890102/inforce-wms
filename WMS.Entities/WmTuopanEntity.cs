using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_tuopan")]
    public class WmTuopanEntity
    {

        ///<summary>
        ///
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///流程状态
        ///</summary>
        public string bpm_status { get; set; }

        ///<summary>
        ///托盘号
        ///</summary>
        public string tin_id { get; set; }

        ///<summary>
        ///托盘顺序
        ///</summary>
        public string tin_sort { get; set; }

        ///<summary>
        ///储位
        ///</summary>
        public string bin_id { get; set; }

        ///<summary>
        ///托盘状态
        ///</summary>
        public string tin_status { get; set; }


    }


}
