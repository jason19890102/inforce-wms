using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 599040 kB; (`id`) REFER `jeecg/t_s_base_user`(`
    ///</summary>
    [Table("t_s_user")]
    public class TSUserEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        public string email { get; set; }

        ///<summary>
        ///手机号
        ///</summary>
        public string mobilePhone { get; set; }

        ///<summary>
        ///办公座机
        ///</summary>
        public string officePhone { get; set; }

        ///<summary>
        ///签名文件
        ///</summary>
        public string signatureFile { get; set; }

        ///<summary>
        ///修改人
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///修改时间
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///修改人id
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建人id
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string user_type { get; set; }


    }


}
