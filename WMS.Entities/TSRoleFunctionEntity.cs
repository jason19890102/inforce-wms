using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 599040 kB; (`roleid`) REFER `jeecg/t_s_role`(`I
    ///</summary>
    [Table("t_s_role_function")]
    public class TSRoleFunctionEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///页面控件权限编码
        ///</summary>
        public string operation { get; set; }

        ///<summary>
        ///菜单ID
        ///</summary>
        public string functionid { get; set; }

        ///<summary>
        ///角色ID
        ///</summary>
        public string roleid { get; set; }

        ///<summary>
        ///数据权限规则ID
        ///</summary>
        public string datarule { get; set; }


    }


}
