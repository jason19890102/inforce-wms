using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_om_notice_h")]
    public class WmOmNoticeHEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///客户
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///要求交货时间
        ///</summary>
        public DateTime? delv_data { get; set; }

        ///<summary>
        ///收货人
        ///</summary>
        public string delv_member { get; set; }

        ///<summary>
        ///收货人电话
        ///</summary>
        public string delv_mobile { get; set; }

        ///<summary>
        ///收货人地址
        ///</summary>
        public string delv_addr { get; set; }

        ///<summary>
        ///承运人
        ///</summary>
        public string re_member { get; set; }

        ///<summary>
        ///承运人电话
        ///</summary>
        public string re_mobile { get; set; }

        ///<summary>
        ///承运人车号
        ///</summary>
        public string re_carno { get; set; }

        ///<summary>
        ///发货月台
        ///</summary>
        public string om_plat_no { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string om_beizhu { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public string om_sta { get; set; }

        ///<summary>
        ///出货单号
        ///</summary>
        public string om_notice_id { get; set; }

        ///<summary>
        ///附件
        ///</summary>
        public string fu_jian { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string READ_ONLY { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string WHERE_CON { get; set; }

        ///<summary>
        ///订单类型
        ///</summary>
        public string order_type_code { get; set; }

        ///<summary>
        ///三方客户
        ///</summary>
        public string ocus_code { get; set; }

        ///<summary>
        ///三方客户名称
        ///</summary>
        public string ocus_name { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string IM_CUS_CODE { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string print_status { get; set; }

        ///<summary>
        ///对接单据类型
        ///</summary>
        public string pi_class { get; set; }

        ///<summary>
        ///账套
        ///</summary>
        public string pi_master { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string delv_method { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string store_code { get; set; }

        ///<summary>
        ///拣货人
        ///</summary>
        public string jh_user { get; set; }

        ///<summary>
        ///复核人
        ///</summary>
        public string fh_user { get; set; }


    }


}
