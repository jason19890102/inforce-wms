using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_cus_cost_h")]
    public class WmCusCostHEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///客户编码
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///开始日期
        ///</summary>
        public DateTime? begin_date { get; set; }

        ///<summary>
        ///结束日期
        ///</summary>
        public DateTime? end_date { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string cus_beizhu { get; set; }

        ///<summary>
        ///合同编号
        ///</summary>
        public string cus_hetongid { get; set; }

        ///<summary>
        ///附件
        ///</summary>
        public string fujian { get; set; }


    }


}
