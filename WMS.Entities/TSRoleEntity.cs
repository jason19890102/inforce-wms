using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_role")]
    public class TSRoleEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///角色编码
        ///</summary>
        public string rolecode { get; set; }

        ///<summary>
        ///角色名字
        ///</summary>
        public string rolename { get; set; }

        ///<summary>
        ///修改人
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///修改时间
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///修改人id
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建人id
        ///</summary>
        public string create_by { get; set; }


    }


}
