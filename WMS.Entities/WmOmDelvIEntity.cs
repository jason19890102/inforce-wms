using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_om_delv_i")]
    public class WmOmDelvIEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///装车复核ID
        ///</summary>
        public string wm_om_delv_id { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///原始单据编码
        ///</summary>
        public string order_id { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string goods_qua { get; set; }

        ///<summary>
        ///物料编码
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///托盘
        ///</summary>
        public string bin_id_to { get; set; }

        ///<summary>
        ///原始单据行项目
        ///</summary>
        public string order_id_i { get; set; }

        ///<summary>
        ///批次
        ///</summary>
        public string goods_batch { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public DateTime? goods_pro_data { get; set; }

        ///<summary>
        ///原始单据类型
        ///</summary>
        public string order_type_code { get; set; }

        ///<summary>
        ///出货单ID
        ///</summary>
        public string wm_om_notice_id { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string delv_beizhu { get; set; }

        ///<summary>
        ///复核状态
        ///</summary>
        public string fh_sta { get; set; }

        ///<summary>
        ///货主
        ///</summary>
        public string cus_code { get; set; }


    }


}
