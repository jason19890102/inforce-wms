using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_cus_cost_i")]
    public class WmCusCostIEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///费用名称
        ///</summary>
        public string cost_code { get; set; }

        ///<summary>
        ///价格RMB
        ///</summary>
        public string cost_jg { get; set; }

        ///<summary>
        ///税率
        ///</summary>
        public string cost_sl { get; set; }

        ///<summary>
        ///折扣
        ///</summary>
        public string cost_zk { get; set; }

        ///<summary>
        ///不含税价RMB
        ///</summary>
        public string cost_bhs { get; set; }

        ///<summary>
        ///含税价RMB
        ///</summary>
        public string cost_hs { get; set; }

        ///<summary>
        ///费用ID
        ///</summary>
        public string cus_cost_id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string free_day { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string free_day2 { get; set; }

        ///<summary>
        ///数据SQL
        ///</summary>
        public string data_sql { get; set; }


    }


}
