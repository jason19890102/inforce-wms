using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///分类管理
    ///</summary>
    [Table("t_s_category")]
    public class TSCategoryEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///图标ID
        ///</summary>
        public string icon_id { get; set; }

        ///<summary>
        ///类型编码
        ///</summary>
        public string code { get; set; }

        ///<summary>
        ///类型名称
        ///</summary>
        public string name { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///上级ID
        ///</summary>
        public string parent_id { get; set; }

        ///<summary>
        ///机构
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///父邮编
        ///</summary>
        public string PARENT_CODE { get; set; }


    }


}
