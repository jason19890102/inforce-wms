﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.UIModels.Response;

namespace WMS.IServices
{
    public interface ILoginServices
    {
        Task<List<AdminUserResponse>> GetByUserNameAsync(string username);
    }
}
