using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_his_stock_ku")]
    public class WmHisStockKuEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        public string id { get; set; }

        ///<summary>
        ///结余日期
        ///</summary>
        public string his_date { get; set; }

        ///<summary>
        ///货主
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///储位
        ///</summary>
        public string ku_wei_bian_ma { get; set; }

        ///<summary>
        ///托盘
        ///</summary>
        public string bin_id { get; set; }

        ///<summary>
        ///物料
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string count { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string base_unit { get; set; }


    }


}
