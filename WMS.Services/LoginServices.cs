﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Core;
using WMS.Data.DAL;
using WMS.IServices;
using WMS.UIModels.Response;

namespace WMS.Services
{
    public class LoginServices: ILoginServices
    {
        async Task<List<AdminUserResponse>> ILoginServices.GetByUserNameAsync(string username)
        {
            var dal = new TSBaseUserDAL();
            var list = await dal.GetByUserNameAsync(username);
            var result = from x in list select ObjMapper.Copy<AdminUserResponse>(x);
            return result.ToList();
        }
    }
}
