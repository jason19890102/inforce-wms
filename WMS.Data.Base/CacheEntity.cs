﻿using System;
using System.Collections.Generic;

using tableAttr = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace WMS.Data.Base
{
    public class CacheEntity
    {
        public Guid ClassId { get; set; }

        public CacheEntity(Type tp)
        {
            this.ClassId = tp.GUID;                             //类id
            this.className = tp.Name;                           //类名

            var attrs = tp.GetCustomAttributes(typeof(tableAttr), true) as tableAttr[];
            if (attrs.Length > 0)
            {
                this.TableName = attrs[0].Name;
            }
            else
            {
                this.TableName = tp.Name;
            }

            this.Fileds = new List<FiledEntity>();
            this.KeyFileds = new List<FiledEntity>();
            this.NormalFileds = new List<FiledEntity>();

            var props = tp.GetProperties();
            foreach (var prop in props)
            {
                var f = new FiledEntity(prop);
                this.Fileds.Add(f);
                if(f.IsKey)
                {
                    this.KeyFileds.Add(f);
                }
                else
                {
                    this.NormalFileds.Add(f);
                }
            }
        }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public string className { get; set; }

        /// <summary>
        /// 全部字段列表
        /// </summary>
        public List<FiledEntity> Fileds { get; private set; }

        /// <summary>
        /// 常规字段列表
        /// </summary>
        public List<FiledEntity> NormalFileds { get; private set; }

        /// <summary>
        /// 主键字段列表
        /// </summary>
        public List<FiledEntity> KeyFileds { get; private set; }

    }

}
