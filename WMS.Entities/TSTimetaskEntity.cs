using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_timetask")]
    public class TSTimetaskEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public string CREATE_BY { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? CREATE_DATE { get; set; }

        ///<summary>
        ///创建人名字
        ///</summary>
        public string CREATE_NAME { get; set; }

        ///<summary>
        ///cron表达式
        ///</summary>
        public string CRON_EXPRESSION { get; set; }

        ///<summary>
        ///是否生效 0/未生效,1/生效
        ///</summary>
        public string IS_EFFECT { get; set; }

        ///<summary>
        ///是否运行0停止,1运行
        ///</summary>
        public string IS_START { get; set; }

        ///<summary>
        ///任务描述
        ///</summary>
        public string TASK_DESCRIBE { get; set; }

        ///<summary>
        ///任务ID
        ///</summary>
        public string TASK_ID { get; set; }

        ///<summary>
        ///修改人
        ///</summary>
        public string UPDATE_BY { get; set; }

        ///<summary>
        ///修改时间
        ///</summary>
        public DateTime? UPDATE_DATE { get; set; }

        ///<summary>
        ///修改人名称
        ///</summary>
        public string UPDATE_NAME { get; set; }


    }


}
