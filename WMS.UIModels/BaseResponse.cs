﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.UIModels
{
    public class BaseResponse
    {
        public string errorMsg { get; set; }
        public bool flag { get; set; }
    }
}
