using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_plat_io")]
    public class WmPlatIoEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///车号
        ///</summary>
        public string carno { get; set; }

        ///<summary>
        ///单据编号
        ///</summary>
        public string doc_id { get; set; }

        ///<summary>
        ///月台编号
        ///</summary>
        public string plat_id { get; set; }

        ///<summary>
        ///进入时间
        ///</summary>
        public DateTime? in_data { get; set; }

        ///<summary>
        ///驶出时间
        ///</summary>
        public DateTime? out_data { get; set; }

        ///<summary>
        ///月台状态
        ///</summary>
        public string plat_sta { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string plat_beizhu { get; set; }

        ///<summary>
        ///计划进入时间
        ///</summary>
        public DateTime? plan_indata { get; set; }

        ///<summary>
        ///计划驶出时间
        ///</summary>
        public DateTime? plan_outdata { get; set; }

        ///<summary>
        ///月台操作
        ///</summary>
        public string plat_oper { get; set; }


    }


}
