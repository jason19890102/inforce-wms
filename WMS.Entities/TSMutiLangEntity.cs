using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_muti_lang")]
    public class TSMutiLangEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///语言主键
        ///</summary>
        public string lang_key { get; set; }

        ///<summary>
        ///内容
        ///</summary>
        public string lang_context { get; set; }

        ///<summary>
        ///语言
        ///</summary>
        public string lang_code { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建人姓名
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///更新人编号
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新人姓名
        ///</summary>
        public string update_name { get; set; }


    }


}
