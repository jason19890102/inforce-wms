using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_sms_template")]
    public class TSSmsTemplateEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///模板类型
        ///</summary>
        public string template_type { get; set; }

        ///<summary>
        ///模板名称
        ///</summary>
        public string template_name { get; set; }

        ///<summary>
        ///模板内容
        ///</summary>
        public string template_content { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }


    }


}
