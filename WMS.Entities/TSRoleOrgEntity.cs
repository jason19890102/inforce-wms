using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_role_org")]
    public class TSRoleOrgEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///机构ID
        ///</summary>
        public string org_id { get; set; }

        ///<summary>
        ///角色ID
        ///</summary>
        public string role_id { get; set; }


    }


}
