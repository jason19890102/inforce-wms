using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_data_rule")]
    public class TSDataRuleEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        public string id { get; set; }

        ///<summary>
        ///数据权限规则名称
        ///</summary>
        public string rule_name { get; set; }

        ///<summary>
        ///字段
        ///</summary>
        public string rule_column { get; set; }

        ///<summary>
        ///条件
        ///</summary>
        public string rule_conditions { get; set; }

        ///<summary>
        ///规则值
        ///</summary>
        public string rule_value { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///修改时间
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///修改人
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///修改人名字
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///菜单ID
        ///</summary>
        public string functionId { get; set; }


    }


}
