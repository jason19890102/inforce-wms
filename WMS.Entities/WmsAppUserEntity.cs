using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wms_app_user")]
    public class WmsAppUserEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///用户编号
        ///</summary>
        public string appuser_code { get; set; }

        ///<summary>
        ///用户名称
        ///</summary>
        public string appuser_name { get; set; }

        ///<summary>
        ///角色id
        ///</summary>
        public string approle_id { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        public string approle_code { get; set; }

        ///<summary>
        ///角色名称
        ///</summary>
        public string approle_name { get; set; }

        ///<summary>
        ///备用1
        ///</summary>
        public string query1 { get; set; }

        ///<summary>
        ///备用2
        ///</summary>
        public string query2 { get; set; }

        ///<summary>
        ///备用3
        ///</summary>
        public string query3 { get; set; }

        ///<summary>
        ///备用4
        ///</summary>
        public string query4 { get; set; }

        ///<summary>
        ///备用5
        ///</summary>
        public string query5 { get; set; }

        ///<summary>
        ///备用6
        ///</summary>
        public string query6 { get; set; }


    }


}
