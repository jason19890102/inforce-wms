using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///通告已读用户表
    ///</summary>
    [Table("t_s_notice_read_user")]
    public class TSNoticeReadUserEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///通告ID
        ///</summary>
        public string notice_id { get; set; }

        ///<summary>
        ///用户ID
        ///</summary>
        public string user_id { get; set; }

        ///<summary>
        ///是否已阅读
        ///</summary>
        public short is_read { get; set; }

        ///<summary>
        ///是否已删除
        ///</summary>
        public short del_flag { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_time { get; set; }


    }


}
