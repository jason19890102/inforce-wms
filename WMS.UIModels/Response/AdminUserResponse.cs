﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.UIModels.Response
{
    public class AdminUserResponse
    {
        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///同步流程
        ///</summary>
        public short? activitiSync { get; set; }

        ///<summary>
        ///浏览器
        ///</summary>
        public string browser { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        public string password { get; set; }

        ///<summary>
        ///真实名字
        ///</summary>
        public string realname { get; set; }

        ///<summary>
        ///签名
        ///</summary>
        public byte[] signature { get; set; }

        ///<summary>
        ///有效状态
        ///</summary>
        public short? status { get; set; }

        ///<summary>
        ///用户KEY
        ///</summary>
        public string userkey { get; set; }

        ///<summary>
        ///用户账号
        ///</summary>
        public string username { get; set; }

        ///<summary>
        ///部门ID
        ///</summary>
        public string departid { get; set; }

        ///<summary>
        ///删除状态
        ///</summary>
        public short? delete_flag { get; set; }

        ///<summary>
        ///微信openid
        ///</summary>
        public string wxopenid { get; set; }
    }
}
