using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_om_delv_h")]
    public class WmOmDelvHEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string delv_beizhu { get; set; }

        ///<summary>
        ///出货单ID
        ///</summary>
        public string wm_om_notice_id { get; set; }

        ///<summary>
        ///复核时间
        ///</summary>
        public DateTime? fh_data { get; set; }

        ///<summary>
        ///货主
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///复核状态
        ///</summary>
        public string fh_sta { get; set; }


    }


}
