﻿using Inforce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Data.DAL
{
    public partial class TSBaseUserDAL
    {
        /// <summary>
        /// 根据username获取用户信息
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<List<TSBaseUserEntity>> GetByUserNameAsync(string username)
        {
            var sqlWhere = $@" WHERE username=@username";
            string sql = "select * from  t_s_base_user" + sqlWhere;
            var result = await this.QueryAsync(sql, new
            {
                username = username
            });
            return result;
        }
    }
}
