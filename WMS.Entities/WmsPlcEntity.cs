using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wms_plc")]
    public class WmsPlcEntity
    {

        ///<summary>
        ///
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///流程状态
        ///</summary>
        public string bpm_status { get; set; }

        ///<summary>
        ///PLCIP
        ///</summary>
        public string plc_ip { get; set; }

        ///<summary>
        ///PLC端口
        ///</summary>
        public string plc_port { get; set; }

        ///<summary>
        ///PLC型号
        ///</summary>
        public string plc_type { get; set; }

        ///<summary>
        ///指令备注
        ///</summary>
        public string com_remark { get; set; }

        ///<summary>
        ///执行时间
        ///</summary>
        public string com_time { get; set; }

        ///<summary>
        ///执行顺序
        ///</summary>
        public string com_seq { get; set; }

        ///<summary>
        ///指令集
        ///</summary>
        public string com_cons { get; set; }

        ///<summary>
        ///备用1
        ///</summary>
        public string remark1 { get; set; }


    }


}
