using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 599040 kB; (`iconid`) REFER `jeecg/t_s_icon`(`I
    ///</summary>
    [Table("t_s_operation")]
    public class TSOperationEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///页面控件code
        ///</summary>
        public string operationcode { get; set; }

        ///<summary>
        ///图标
        ///</summary>
        public string operationicon { get; set; }

        ///<summary>
        ///页面名字
        ///</summary>
        public string operationname { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public short? status { get; set; }

        ///<summary>
        ///菜单ID
        ///</summary>
        public string functionid { get; set; }

        ///<summary>
        ///图标ID
        ///</summary>
        public string iconid { get; set; }

        ///<summary>
        ///规则类型：1/禁用 0/隐藏
        ///</summary>
        public short? operationtype { get; set; }


    }


}
