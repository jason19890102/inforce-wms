﻿namespace WMS.Core
{
    public class emCache<TKey, TValue>
    {
        private static readonly object mutex = new object();

        public SafeDictionary<TKey, TValue> Datas { get; private set; } 
            = new SafeDictionary<TKey, TValue>();

        protected emCache()
        {
        }

        private static emCache<TKey, TValue> _instance = null;

        public static emCache<TKey, TValue> Instance
        {
            get
            {
                lock (mutex)
                {
                    if (_instance == null) _instance = new emCache<TKey, TValue>();
                    return _instance;
                }
            }
        }


    }
}
