using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_depart")]
    public class TSDepartEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///部门名称
        ///</summary>
        public string departname { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        public string description { get; set; }

        ///<summary>
        ///父部门ID
        ///</summary>
        public string parentdepartid { get; set; }

        ///<summary>
        ///机构编码
        ///</summary>
        public string org_code { get; set; }

        ///<summary>
        ///机构类型
        ///</summary>
        public string org_type { get; set; }

        ///<summary>
        ///手机号
        ///</summary>
        public string mobile { get; set; }

        ///<summary>
        ///传真
        ///</summary>
        public string fax { get; set; }

        ///<summary>
        ///地址
        ///</summary>
        public string address { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public string depart_order { get; set; }


    }


}
