using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///每日余额
    ///</summary>
    [Table("wm_day_his")]
    public class WmDayHisEntity
    {

        ///<summary>
        ///
        ///</summary>
        public string id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string his_date { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string his_day_in { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string his_day_out { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string his_day_amount { get; set; }


    }


}
