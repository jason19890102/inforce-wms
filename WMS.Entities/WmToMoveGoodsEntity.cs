using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_to_move_goods")]
    public class WmToMoveGoodsEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///原始单据类型
        ///</summary>
        public string order_type_code { get; set; }

        ///<summary>
        ///原始单据编码
        ///</summary>
        public string order_id { get; set; }

        ///<summary>
        ///原始单据行项目
        ///</summary>
        public string order_id_i { get; set; }

        ///<summary>
        ///物料编码
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///物料名称
        ///</summary>
        public string goods_name { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string goods_qua { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public string goods_pro_data { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///客户编码
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///客户名称
        ///</summary>
        public string cus_name { get; set; }

        ///<summary>
        ///源托盘
        ///</summary>
        public string tin_from { get; set; }

        ///<summary>
        ///到托盘
        ///</summary>
        public string tin_id { get; set; }

        ///<summary>
        ///源储位
        ///</summary>
        public string bin_from { get; set; }

        ///<summary>
        ///到储位
        ///</summary>
        public string bin_to { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public string move_sta { get; set; }

        ///<summary>
        ///转移客户
        ///</summary>
        public string to_cus_code { get; set; }

        ///<summary>
        ///转移客户名称
        ///</summary>
        public string to_cus_name { get; set; }

        ///<summary>
        ///基本单位
        ///</summary>
        public string base_unit { get; set; }

        ///<summary>
        ///基本单位数量
        ///</summary>
        public string base_goodscount { get; set; }

        ///<summary>
        ///到生产日期
        ///</summary>
        public string to_goods_pro_data { get; set; }

        ///<summary>
        ///执行状态
        ///</summary>
        public string run_sta { get; set; }


    }


}
