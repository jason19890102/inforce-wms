using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///通告授权用户表
    ///</summary>
    [Table("t_s_notice_authority_user")]
    public class TSNoticeAuthorityUserEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///通告ID
        ///</summary>
        public string notice_id { get; set; }

        ///<summary>
        ///授权用户ID
        ///</summary>
        public string user_id { get; set; }


    }


}
