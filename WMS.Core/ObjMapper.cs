﻿using Newtonsoft.Json;

namespace WMS.Core
{
    public static class ObjMapper
    {
        public static T Copy<T>(object arg)
        {
            var json = JsonConvert.SerializeObject(arg);
            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }
    }
}
