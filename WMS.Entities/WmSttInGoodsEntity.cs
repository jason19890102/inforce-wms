using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_stt_in_goods")]
    public class WmSttInGoodsEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///库位编码
        ///</summary>
        public string bin_id { get; set; }

        ///<summary>
        ///托盘编码
        ///</summary>
        public string tin_id { get; set; }

        ///<summary>
        ///物料编码
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///物料名称
        ///</summary>
        public string goods_name { get; set; }

        ///<summary>
        ///数量
        ///</summary>
        public string goods_qua { get; set; }

        ///<summary>
        ///单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public string goods_pro_data { get; set; }

        ///<summary>
        ///批次
        ///</summary>
        public string goods_batch { get; set; }

        ///<summary>
        ///盘点数量
        ///</summary>
        public string stt_qua { get; set; }

        ///<summary>
        ///客户名称
        ///</summary>
        public string cus_name { get; set; }

        ///<summary>
        ///客户
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///盘点状态
        ///</summary>
        public string stt_sta { get; set; }

        ///<summary>
        ///基本单位
        ///</summary>
        public string base_unit { get; set; }

        ///<summary>
        ///基本单位数量
        ///</summary>
        public string base_goodscount { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string stt_id { get; set; }

        ///<summary>
        ///物料统一编码
        ///</summary>
        public string goods_code { get; set; }

        ///<summary>
        ///盘点类型
        ///</summary>
        public string stt_type { get; set; }

        ///<summary>
        ///动线
        ///</summary>
        public string dong_xian { get; set; }


    }


}
