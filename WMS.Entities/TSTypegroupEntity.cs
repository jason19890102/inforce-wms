using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_typegroup")]
    public class TSTypegroupEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///字典分组编码
        ///</summary>
        public string typegroupcode { get; set; }

        ///<summary>
        ///字典分组名称
        ///</summary>
        public string typegroupname { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建用户
        ///</summary>
        public string create_name { get; set; }


    }


}
