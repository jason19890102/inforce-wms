using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_data_source")]
    public class TSDataSourceEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///多数据源KEY
        ///</summary>
        public string db_key { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        public string description { get; set; }

        ///<summary>
        ///驱动class
        ///</summary>
        public string driver_class { get; set; }

        ///<summary>
        ///db链接
        ///</summary>
        public string url { get; set; }

        ///<summary>
        ///用户名
        ///</summary>
        public string db_user { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        public string db_password { get; set; }

        ///<summary>
        ///数据库类型
        ///</summary>
        public string db_type { get; set; }

        ///<summary>
        ///数据源名字
        ///</summary>
        public string db_name { get; set; }


    }


}
