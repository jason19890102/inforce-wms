using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 599040 kB; (`userid`) REFER `jeecg/t_s_user`(`i
    ///</summary>
    [Table("t_s_role_user")]
    public class TSRoleUserEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///角色ID
        ///</summary>
        public string roleid { get; set; }

        ///<summary>
        ///用户ID
        ///</summary>
        public string userid { get; set; }


    }


}
