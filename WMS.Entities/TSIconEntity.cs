using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_icon")]
    public class TSIconEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///图片后缀
        ///</summary>
        public string extend { get; set; }

        ///<summary>
        ///类型
        ///</summary>
        public string iconclas { get; set; }

        ///<summary>
        ///图片流内容
        ///</summary>
        public byte[] content { get; set; }

        ///<summary>
        ///名字
        ///</summary>
        public string name { get; set; }

        ///<summary>
        ///路径
        ///</summary>
        public string path { get; set; }

        ///<summary>
        ///类型 1系统图标/2菜单图标/3桌面图标
        ///</summary>
        public short? type { get; set; }


    }


}
