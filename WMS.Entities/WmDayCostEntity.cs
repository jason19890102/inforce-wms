using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_day_cost")]
    public class WmDayCostEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///客户
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///客户名称
        ///</summary>
        public string cus_name { get; set; }

        ///<summary>
        ///费用
        ///</summary>
        public string cost_code { get; set; }

        ///<summary>
        ///费用名称
        ///</summary>
        public string cost_name { get; set; }

        ///<summary>
        ///费用日期
        ///</summary>
        public DateTime? cost_data { get; set; }

        ///<summary>
        ///每日费用
        ///</summary>
        public string day_cost_yj { get; set; }

        ///<summary>
        ///不含税价
        ///</summary>
        public string day_cost_bhs { get; set; }

        ///<summary>
        ///税额
        ///</summary>
        public string day_cost_se { get; set; }

        ///<summary>
        ///含税价
        ///</summary>
        public string day_cost_hsj { get; set; }

        ///<summary>
        ///费用来源
        ///</summary>
        public string cost_ori { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string beizhu { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public string cost_sta { get; set; }

        ///<summary>
        ///计费数量
        ///</summary>
        public string cost_sl { get; set; }

        ///<summary>
        ///数量单位
        ///</summary>
        public string cost_unit { get; set; }

        ///<summary>
        ///是否已结算
        ///</summary>
        public string cost_js { get; set; }


    }


}
