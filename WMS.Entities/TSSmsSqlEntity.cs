using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_sms_sql")]
    public class TSSmsSqlEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///SQL名称
        ///</summary>
        public string sql_name { get; set; }

        ///<summary>
        ///SQL内容
        ///</summary>
        public string sql_content { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }


    }


}
