﻿namespace WMS.Configuration
{
    public class ConfigDictItem
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}