using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 599040 kB; (`typegroupid`) REFER `jeecg/t_s_typ
    ///</summary>
    [Table("t_s_type")]
    public class TSTypeEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///字典编码
        ///</summary>
        public string typecode { get; set; }

        ///<summary>
        ///字典名称
        ///</summary>
        public string typename { get; set; }

        ///<summary>
        ///无用字段
        ///</summary>
        public string typepid { get; set; }

        ///<summary>
        ///字典组ID
        ///</summary>
        public string typegroupid { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///创建用户
        ///</summary>
        public string create_name { get; set; }


    }


}
