using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_sms")]
    public class TSSmsEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///消息标题
        ///</summary>
        public string es_title { get; set; }

        ///<summary>
        ///消息类型
        ///</summary>
        public string es_type { get; set; }

        ///<summary>
        ///发送人
        ///</summary>
        public string es_sender { get; set; }

        ///<summary>
        ///接收人
        ///</summary>
        public string es_receiver { get; set; }

        ///<summary>
        ///内容
        ///</summary>
        public string es_content { get; set; }

        ///<summary>
        ///发送时间
        ///</summary>
        public DateTime? es_sendtime { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string remark { get; set; }

        ///<summary>
        ///发送状态
        ///</summary>
        public string es_status { get; set; }


    }


}
