﻿using System.Collections.Generic;

namespace WMS.Data.Base
{
    public class PagedList<T>
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public long RecordCount { get; set; }

        public List<T> Datas { get; set; } = new List<T>();
    }
}
