using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wms_wave_conf")]
    public class WmsWaveConfEntity
    {

        ///<summary>
        ///
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///流程状态
        ///</summary>
        public string bpm_status { get; set; }

        ///<summary>
        ///配送点
        ///</summary>
        public string peisondian { get; set; }

        ///<summary>
        ///波次类型
        ///</summary>
        public string wave_type { get; set; }

        ///<summary>
        ///备用1
        ///</summary>
        public string wv_by1 { get; set; }

        ///<summary>
        ///备用2
        ///</summary>
        public string wv_by2 { get; set; }

        ///<summary>
        ///备用3
        ///</summary>
        public string wv_by3 { get; set; }


    }


}
