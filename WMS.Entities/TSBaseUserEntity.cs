using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///InnoDB free: 600064 kB; (`departid`) REFER `jeecg/t_s_depart
    ///</summary>
    [Table("t_s_base_user")]
    public class TSBaseUserEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///同步流程
        ///</summary>
        public short? activitiSync { get; set; }

        ///<summary>
        ///浏览器
        ///</summary>
        public string browser { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        public string password { get; set; }

        ///<summary>
        ///真实名字
        ///</summary>
        public string realname { get; set; }

        ///<summary>
        ///签名
        ///</summary>
        public byte[] signature { get; set; }

        ///<summary>
        ///有效状态
        ///</summary>
        public short? status { get; set; }

        ///<summary>
        ///用户KEY
        ///</summary>
        public string userkey { get; set; }

        ///<summary>
        ///用户账号
        ///</summary>
        public string username { get; set; }

        ///<summary>
        ///部门ID
        ///</summary>
        public string departid { get; set; }

        ///<summary>
        ///删除状态
        ///</summary>
        public short? delete_flag { get; set; }

        ///<summary>
        ///微信openid
        ///</summary>
        public string wxopenid { get; set; }


    }


}
