using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("t_s_user_org")]
    public class TSUserOrgEntity
    {

        ///<summary>
        ///id
        ///</summary>
        [Key]
        public string ID { get; set; }

        ///<summary>
        ///用户id
        ///</summary>
        public string user_id { get; set; }

        ///<summary>
        ///部门id
        ///</summary>
        public string org_id { get; set; }


    }


}
