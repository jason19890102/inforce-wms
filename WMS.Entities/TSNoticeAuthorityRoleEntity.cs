using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///通告授权角色表
    ///</summary>
    [Table("t_s_notice_authority_role")]
    public class TSNoticeAuthorityRoleEntity
    {

        ///<summary>
        ///ID
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///通告ID
        ///</summary>
        public string notice_id { get; set; }

        ///<summary>
        ///授权角色ID
        ///</summary>
        public string role_id { get; set; }


    }


}
