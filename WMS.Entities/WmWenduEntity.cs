using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_wendu")]
    public class WmWenduEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///流程状态
        ///</summary>
        public string bpm_status { get; set; }

        ///<summary>
        ///温度地点
        ///</summary>
        public string wendu_dd { get; set; }

        ///<summary>
        ///采集时间
        ///</summary>
        public DateTime? wendu_cjsj { get; set; }

        ///<summary>
        ///温度值摄氏度
        ///</summary>
        public string wendu_zhi { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string wendu_bz { get; set; }


    }


}
