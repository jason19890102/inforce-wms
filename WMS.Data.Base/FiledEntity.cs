﻿using System;
using System.Reflection;

using columnAttr = System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
using KeyAttr = System.ComponentModel.DataAnnotations.KeyAttribute;

namespace WMS.Data.Base
{
    /// <summary>
    /// 表字段类
    /// </summary>
    public class FiledEntity
    {

        /// <summary>
        /// 属性信息
        /// </summary>
        public PropertyInfo Prop { get; private set; }

        /// <summary>
        /// 表字段名
        /// </summary>
        public string FieldName { get; private set; }

        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsKey { get; private set; } = false;

        /// <summary>
        /// 属性名
        /// </summary>
        public string propName { get; private set; }

        public FiledEntity(PropertyInfo prop)
        {
            this.Prop = prop;
            this.propName = this.Prop.Name;

            var keyAttr = prop.GetCustomAttribute<KeyAttr>();
            this.IsKey = (keyAttr != null);

            var colAttr = prop.GetCustomAttribute<columnAttr>();
            this.FieldName = colAttr?.Name ?? this.propName;
        }

    }
}