﻿using WMS.Configuration;
using WMS.Core;
using WMS.Data.Base;

namespace WMS.Data.DAL
{
    public class BaseWMSDAL<T> : BaseDAL<T>
    {
        public BaseWMSDAL() : base(ConfigManager.Instance.WMSDB)
        {

        }
    }
}
