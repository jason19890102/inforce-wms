﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS.UIModels;
using WMS.UIModels.Response;

namespace WMS.WebSite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {

        private readonly IServices.ILoginServices _LoginServices;
        public HomeController()
        {
            _LoginServices = new Services.LoginServices();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<ModelsResponse> Login(string username)
        {
            try
            {
                var resultJson = await _LoginServices.GetByUserNameAsync(username);
                var Response = new ModelsResponse();
                Response.flag = true;
                Response.resultData = JsonConvert.SerializeObject(resultJson);
                return Response;
            }
            catch (Exception ex)
            {
                 var Response = new ModelsResponse();
                 Response.flag = false;
                 Response.errorMsg = ex.Message;
                 return Response;
            }
        }
    }
}
