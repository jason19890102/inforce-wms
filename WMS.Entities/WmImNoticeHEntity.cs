using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_im_notice_h")]
    public class WmImNoticeHEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///客户编码
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///预计到货时间
        ///</summary>
        public DateTime? im_data { get; set; }

        ///<summary>
        ///客户订单号
        ///</summary>
        public string im_cus_code { get; set; }

        ///<summary>
        ///司机
        ///</summary>
        public string im_car_dri { get; set; }

        ///<summary>
        ///司机电话
        ///</summary>
        public string im_car_mobile { get; set; }

        ///<summary>
        ///车号
        ///</summary>
        public string im_car_no { get; set; }

        ///<summary>
        ///订单类型
        ///</summary>
        public string order_type_code { get; set; }

        ///<summary>
        ///月台
        ///</summary>
        public string platform_code { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string im_beizhu { get; set; }

        ///<summary>
        ///单据状态
        ///</summary>
        public string im_sta { get; set; }

        ///<summary>
        ///进货通知单号
        ///</summary>
        public string notice_id { get; set; }

        ///<summary>
        ///附件
        ///</summary>
        public string FU_JIAN { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string READ_ONLY { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string WHERE_CON { get; set; }

        ///<summary>
        ///供应商编码
        ///</summary>
        public string sup_code { get; set; }

        ///<summary>
        ///供应商名称
        ///</summary>
        public string sup_name { get; set; }

        ///<summary>
        ///对接单据类型
        ///</summary>
        public string pi_class { get; set; }

        ///<summary>
        ///账套
        ///</summary>
        public string pi_master { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string area_code { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string store_code { get; set; }


    }


}
