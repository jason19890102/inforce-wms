using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inforce.Entities
{

    ///<summary>
    ///
    ///</summary>
    [Table("wm_om_notice_i")]
    public class WmOmNoticeIEntity
    {

        ///<summary>
        ///主键
        ///</summary>
        [Key]
        public string id { get; set; }

        ///<summary>
        ///创建人名称
        ///</summary>
        public string create_name { get; set; }

        ///<summary>
        ///创建人登录名称
        ///</summary>
        public string create_by { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime? create_date { get; set; }

        ///<summary>
        ///更新人名称
        ///</summary>
        public string update_name { get; set; }

        ///<summary>
        ///更新人登录名称
        ///</summary>
        public string update_by { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        public DateTime? update_date { get; set; }

        ///<summary>
        ///所属部门
        ///</summary>
        public string sys_org_code { get; set; }

        ///<summary>
        ///所属公司
        ///</summary>
        public string sys_company_code { get; set; }

        ///<summary>
        ///出货通知ID
        ///</summary>
        public string om_notice_id { get; set; }

        ///<summary>
        ///出货物料
        ///</summary>
        public string goods_id { get; set; }

        ///<summary>
        ///出货数量
        ///</summary>
        public string goods_qua { get; set; }

        ///<summary>
        ///出货单位
        ///</summary>
        public string goods_unit { get; set; }

        ///<summary>
        ///生产日期
        ///</summary>
        public DateTime? goods_pro_data { get; set; }

        ///<summary>
        ///批次
        ///</summary>
        public string goods_batch { get; set; }

        ///<summary>
        ///出货仓位
        ///</summary>
        public string bin_om { get; set; }

        ///<summary>
        ///已出货数量
        ///</summary>
        public string goods_quaok { get; set; }

        ///<summary>
        ///预约出货时间
        ///</summary>
        public string delv_data { get; set; }

        ///<summary>
        ///客户
        ///</summary>
        public string cus_code { get; set; }

        ///<summary>
        ///客户名称
        ///</summary>
        public string cus_name { get; set; }

        ///<summary>
        ///物料名称
        ///</summary>
        public string goods_text { get; set; }

        ///<summary>
        ///波次号
        ///</summary>
        public string wave_id { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public string om_sta { get; set; }

        ///<summary>
        ///基本单位
        ///</summary>
        public string base_unit { get; set; }

        ///<summary>
        ///基本单位数量
        ///</summary>
        public string base_goodscount { get; set; }

        ///<summary>
        ///下架计划生成
        ///</summary>
        public string plan_sta { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string goods_name { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string other_id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string bin_id { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string IM_CUS_CODE { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string OM_BEI_ZHU { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string BZHI_QI { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string chp_shu_xing { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string BARCODE { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string sku { get; set; }


    }


}
